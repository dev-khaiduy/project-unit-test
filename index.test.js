import MyBigNumber from '../Project/index.js';//import MyBigNumber from index.js';

function testSum(stn1, stn2, expectedResult) {
    const result = MyBigNumber.sum(stn1, stn2);
    if (result !== expectedResult) {
      console.error(`Test failed with input (${stn1}, ${stn2}). Expected ${expectedResult}, but got ${result}.`);
    } else {
      console.log(`Test passed with input (${stn1}, ${stn2}).`);
    }
  }


//Chạy các case test
testSum('1', '2','3') ; 
testSum('12', '12','24') ; 
testSum('123', '123','246') ; 
testSum('1234', '4321','5555') ; 
testSum('123456789', '987654321','1111111110') ; 
testSum('1', '2','5') ; 