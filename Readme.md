# Hướng dẫn chạy các test case

**Yêu cầu**
Node.js version 14 trở lên.

**Các bước thực hiện**

# 1. Tải mã nguồn từ Github:
git clone https://gitlab.com/dev-khaiduy/project-unit-test.git

# 2. Chuyển vào thư mục chứa mã nguồn:
cd your_repository

# 3. Cài đặt các thư viện cần thiết:
npm install

# 4. Chạy tất cả các test case:
npm start


Nếu có bất kỳ test case nào bị lỗi, thông báo lỗi sẽ được hiển thị.

**Thông tin chi tiết về các test case**
Các test case được định nghĩa trong file index.test.js.

Mỗi test case là một hàm, với tên bắt đầu bằng từ test. Mỗi test case bao gồm các bước sau:

Chuẩn bị dữ liệu đầu vào.
Gọi hàm cần kiểm tra với dữ liệu đầu vào đã chuẩn bị.
Kiểm tra kết quả trả về của hàm với kết quả mong đợi.
Hiển thị thông báo kết quả đúng/sai.
Bạn có thể thêm, sửa hoặc xóa các test case trong file index.test.js để kiểm tra tính đúng đắn của lớp MyBigNumber.

